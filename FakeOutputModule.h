/*
 * FakeOutputModule.h
 *
 *  Created on: 25/feb/2015
 *      Author: user
 */

#ifndef FAKEOUTPUTMODULE_H_
#define FAKEOUTPUTMODULE_H_

#include <string>
#include "dq_include.h"
using namespace std;

class FakeOutputModule {
private:
	string _name;
public:
	FakeOutputModule();
	virtual ~FakeOutputModule();
	string getName();
	void startNotify();
};

#endif /* FAKEOUTPUTMODULE_H_ */
