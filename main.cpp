/*
 * main.cpp
 *
 *  Created on: 25/feb/2015
 *      Author: user
 */

#include <iostream>
#include "dq_include.h"
#include "DQTopcon.h"
#include "FakeInputModule.h"
#include "FakeOutputModule.h"
using namespace std;

FakeInputModule *inModule;
FakeOutputModule *outModule;
sem_t mySemaphore;

DQSignal allTopconSignals[17] = { dq_posizioneIdTipo_in_signal,
			dq_posizioneIdDispositivo_in_signal, dq_posizioneTimestamp_in_signal,
			dq_posizioneLongitudine_in_signal, dq_posizioneLatitudine_in_signal,
			dq_posizioneAltitudine_in_signal, dq_posizioneHeading_in_signal,
			dq_posizioneVelocita_in_signal, dq_posizioneMetriPercorsi_in_signal,
			dq_posizioneMetriTotali_in_signal, dq_statisticaIdTipo_in_signal,
			dq_statisticaValore_in_signal, dq_statisticaMessaggio_in_signal,
			dq_flexIdTipo_in_signal, dq_flexMotore_in_signal,
			dq_flexBytes_in_signal, dq_flexValore_in_signal };

int main(int argc, char **argv) {

	DQ_init((const char*)dq_topcon_developer_key);
	inModule = new FakeInputModule();
	outModule = new FakeOutputModule();

	for (int i = 0; i < 17; ++i) {
		DQ_addSignal(allTopconSignals[i], outModule->getName().c_str(), inModule->getName().c_str());
	}

	sem_init(&mySemaphore, 0, 1);

	outModule->startNotify();
	inModule->start();


	cout << "Start procedure" << endl;

	for (; ;) {

		sem_wait(&mySemaphore);
		DQ_manager(0);
		sem_post(&mySemaphore);

	}

	return 0;

}



