/*
 * FakeOutputModule.cpp
 *
 *  Created on: 25/feb/2015
 *      Author: user
 */

#include <iostream>
#include "FakeOutputModule.h"
#include "DQTopcon.h"

void outNotify(DQSignal* signals, uint8_t n_signals) {

	/*
	 * To send signals to the SDK call DQ_packSignals
	 */
//	uint8_t packet[500];
//	uint16_t len = 0;
//	uint8_t packRet;
//	packRet = DQ_packSignals(_name.c_str(), signals, n_signals, packet, &len, 500);

	for (int i = 0; i < n_signals; ++i) {
		DQSignal s = signals[i];
		long long int data; // = ((uint16_t) s.pData[0] << 8) | s.pData[1];
		switch (s.maxLen) {
		case 1: {
			data = s.pData[0];
			break;
		}
		case 2: {
			data = ((uint16_t) s.pData[0] << 8) | s.pData[1];
			break;
		}
		case 4: {
			data =
					((((uint32_t) s.pData[0] << 24)
							| (uint32_t) s.pData[1] << 16)
							| (uint32_t) s.pData[2] << 8) | s.pData[3];
			break;
		}
		case 6: {
			//TODO:
			break;
		}
		default:
			break;
		}
		cout << "Signal name: " << s.name << " Signal ID: " << s.id
				<< " Data len: " << (int) s.maxLen << "  Signal data: " << data
				<< endl;
	}
}

FakeOutputModule::FakeOutputModule() {
	_name = "FakeOut";
	DQ_initModule(_name.c_str(), DQ_STANDALONE_MODULE);


	///////////////////////////
	// 		USE WITH SDK	 //
	//////////////////////////

	/*
	 * To communicate with DQuid SDK you have to set the module to act as client
	 */
	//DQ_initModule(_name.c_str(), DQ_CLIENT_MODULE);

	/*
	 * At the beginning of the communication you have to generate a challenge and sent it to the SDK
	 */
	//uint8_t challenge[128];
	//DQ_startChallenge(challenge, _name.c_str());

	/*
	 * The SDK will send a 64 bytes signature. Verify this message.
	 */
	//DQ_verify(mySignature, _name.c_str());

	/*
	 * Now for every message that you will receive from SDK call DQ_onModuleReceivedMsg pasing the data
	 */
	//DQ_onModuleReceivedMsg(_name.c_str(), data, dataLen);

	DQ_registerCallback(_name.c_str(),
			(DQ_signalNotificationCallback) outNotify);
}

FakeOutputModule::~FakeOutputModule() {
}

string FakeOutputModule::getName() {
	return _name;
}

void FakeOutputModule::startNotify() {
	cout << "FakeOutputModule::startNotify" << endl;
	for (int i = 0; i < 17; ++i) {
		DQ_addNotifyModuleForSignal(allTopconSignals[i].name, _name.c_str());
	}
}

