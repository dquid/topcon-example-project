/*
 * FakeInputModule.h
 *
 *  Created on: 25/feb/2015
 *      Author: user
 */

#ifndef FAKEINPUTMODULE_H_
#define FAKEINPUTMODULE_H_

#include <string>
using namespace std;

class FakeInputModule {
private:
	string _name;
public:
	FakeInputModule();
	virtual ~FakeInputModule();
	string getName();
	void start();
};

#endif /* FAKEINPUTMODULE_H_ */
