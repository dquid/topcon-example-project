/*
 * FakeInputModule.cpp
 *
 *  Created on: 25/feb/2015
 *      Author: user
 */

#include "FakeInputModule.h"
#include "dq_include.h"
#include "DQTopcon.h"
#include <pthread.h>
#include <iostream>
#include <stdlib.h>
#include <time.h>

void* doSomething(void *arg) {
	cout << "Started thread" << endl;
	srand(time(NULL));

	while (1) {
		int r = rand() % 17;

		sem_wait(&mySemaphore);
		switch (allTopconSignals[r].maxLen) {
		case 1: {
			uint8_t data = rand() % 200;
			cout << "DATO: " << data << endl;
			DQ_updateSignalValue(allTopconSignals[r].name, (uint8_t*) &data, 1,
					DB_ITEM_FORCE_UPDATE_NO, "FakeIn");
			break;
		}
		case 2: {
			uint16_t data = rand() % 16000;
			cout << "DATO: " << data << endl;
			DQ_updateSignalValue_Uint16(allTopconSignals[r].name, data,
					DB_ITEM_FORCE_UPDATE_NO, "FakeIn");
			break;
		}
		case 4: {
			uint32_t data = rand() % 16000;
			cout << "DATO: " << data << endl;
			DQ_updateSignalValue_Uint32(allTopconSignals[r].name, data,
					DB_ITEM_FORCE_UPDATE_NO, "FakeIn");
			break;
		}
		case 6: {
			uint64_t data = rand() % 16000;
			cout << "DATO: " << data << endl;
			DQ_updateSignalValue(allTopconSignals[r].name, (uint8_t*) &data, 6,
					DB_ITEM_FORCE_UPDATE_NO, "FakeIn");
			break;
		}
		default:
			break;
		}

		sem_post(&mySemaphore);

		usleep(5000);
	}

	return 0;
}

FakeInputModule::FakeInputModule() {
	_name = "FakeIn";
	DQ_initModule(_name.c_str(), DQ_STANDALONE_MODULE);
}

FakeInputModule::~FakeInputModule() {
}

string FakeInputModule::getName() {
	return _name;
}

void FakeInputModule::start() {
	pthread_t my_thread;
	int ret;
	ret = pthread_create(&my_thread, NULL, &doSomething, NULL);
	if (ret != 0) {
		cout << "Error: pthread_create() failed\n";
	}

	return;

}

